<?php
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/requirements.php');
	require_once('controller/view.php');

	if (isset($_SESSION['user'])) header('Location: /');

	if (isset($_GET['action'])) $action = $_GET['action'];
	else $action = 'default';

	view($action);

	$APP = array(
		"name" => "Login",
	);
	$HEADASSETS = '<link rel="stylesheet" href="css/login.css" />';

	include_once($path.'/view/main.php');
