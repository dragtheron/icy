<?php
	global $MAINCONTENT;
	global $ERRORCONTENT;

	function login($user, $pass) {
		$rawUser = databaseAccess("SELECT * FROM users WHERE user = '".$user."' AND pass = '".$pass."'");
		$result = $rawUser->fetch_assoc();
		$_SESSION['user'] = $result['id'];
		$_SESSION['user_name'] = $result['name'];
		$_SESSION['admin'] = $result['admin'];
		if ($_SESSION['user'] != '') {
			header('Location: /index.php');
		}
		else {
			viewError('LOGIN001');
			viewMain();
		}
	}

	function view($action) {
		switch($action) {
			case 'login':
				if (isset($_POST['username']) && isset($_POST['password']))
					login($_POST['username'], $_POST['password']);
				break;
			case 'logout':
				session_destroy();
				header('Location: /');
				break;
			default:
				viewMain();
				break;
		}
	}

	function viewError($error) {
		switch($error) {
			case 'LOGIN001': pushError($error, 'Invalid username or password.');
		}
	}

	function viewMain() {
		pushContent("<section>");
			pushContent("<p class='title'>Please enter your login credentials</p>");
			pushContent("<form id='login' method='post' action='index.php?action=login'>");
				pushContent("<input type='text' name='username' placeholder='username'>");
				pushContent("<input type='password' name='password' placeholder='password'>");
				pushContent("<input type='submit' value='Login'>");
			pushContent("</form>");
		pushContent("</section>");
	}
