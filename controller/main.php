<?php
	function adminCheck($user) {
		$rawUser = databaseAccess("SELECT * FROM users WHERE id = ".$user);
		$result = $rawUser->fetch_assoc();
		if ($result['admin'] == 1) return true;
		else return false;
	}

	function pushContent($content) {
		global $MAINCONTENT;
		$MAINCONTENT .= $content;
	}

	function pushError($error, $content) {
		global $ERRORCONTENT;
		$ERRORCONTENT .= "<section>";
		$ERRORCONTENT .= $content."<br><span class='identifier'>[".$error."]</span><br>";
		$ERRORCONTENT .= "</section>";
	}

	function pushMenu($content) {
		global $MENU;
		$MENU .= "<div class='menu'>";
		$MENU .= $content;
		$MENU .= "</div>";
	}
