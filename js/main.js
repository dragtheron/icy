/* main javascripts for Project Icy */

var switchNavigation = function() {
	if ($('aside').hasClass('nav-opened')) {
		$('aside').switchClass('nav-opened', 'nav-closed', 500);
		$('header').switchClass('nav-opened', 'nav-closed', 500);
		$('main').switchClass('nav-opened', 'nav-closed', 500);
		$('.menu').switchClass('nav-opened', 'nav-closed', 500);
	}
	else {
		$('aside').switchClass('nav-closed', 'nav-opened', 500);
		$('header').switchClass('nav-closed', 'nav-opened', 500);
		$('main').switchClass('nav-closed', 'nav-opened', 500);
		$('.menu').switchClass('nav-closed', 'nav-opened', 500);
	}
}

$('#nav-opener').click(switchNavigation);
$('html').on('click', function() {
	if ($('aside').hasClass('nav-opened')) {
		$('aside').switchClass('nav-opened', 'nav-closed', 500);
		$('header').switchClass('nav-opened', 'nav-closed', 500);
		$('main').switchClass('nav-opened', 'nav-closed', 500);
		$('.menu').switchClass('nav-opened', 'nav-closed', 500);
	}
});

$(document).ready(function(){
    $('.autoresize').autosize();
});

/*
$('main').scroll(function () {
	if ($('main').scrollTop() > 20)
		$('body>header').css('background-color', '#eee');
	else
		$('body>header').css('background-color', '#fff');
});
*/


function confirmSubmit(extra) {
  var agree=confirm(extra+"\n"+"Are you sure you want to do this?");
  if (agree) return true ;
  else return false ;
}
