<?php
	set_time_limit(0);
	require_once('../requirements.php');
	
	$imagelist = array();
	$imagelist = glob('predefined/*.*{jpg,png,gif,jpeg}', GLOB_BRACE);
	foreach ($imagelist as $image) {
	  convertAndBlur($image);
	}
	
	
	if (isset($_GET['option']) && $_GET['option'] == 'predefined') {
	  $imagelist = glob('predefined/*.*{jpg,png,gif,jpeg}', GLOB_BRACE);
	  foreach ($imagelist as $image) {
	    convertAndBlur($image);
	  }
	}
	
	function convertAndBlur($image) {
	   $id = uniqid();
	   if (extension_loaded('imagick') && (!isset($_GET['option']) && $_GET['option'] != 'map')) {
	     $img = new Imagick($image);
	     $img->setImageFormat('jpeg');
	     $img->setImageCompressionQuality(100);
	     $img->writeImage('predefined/_upload/converted_' . $id . ".jpg");
	     unlink($image);
	
	     $imgBlur = clone $img;
	     $imgBlur->blurImage(0,50);
	
	     $imgBlur->writeImage('predefined/_blur/' . $id . ".jpg");
	
	     $img->clear(); $img->destroy();
	     $imgBlur->clear(); $imgBlur->destroy();
	     databaseAccess("INSERT INTO images (name, origin) VALUES ('converted_".$id."', '".$image.".jpg')");
	   } else {
	     databaseAccess("REPLACE INTO images (name, origin) VALUES ('".$image."', '".$image."')");
	    echo ("REPLACE INTO images (name, origin) VALUES ('".$image."', '".$image."')")  ;
	   }
	}
