<?php
	function databaseAccess($query) {
		
		$host = "";
		$user = "";
		$pass = "";

		$base = "";


		$mysqli = new mysqli($host, $user, $pass, $base);
		if (mysqli_connect_error())
			die("Database connection failed [<b>".mysqli_connect_errno()."</b>]: ".mysqli_connect_error());

		mysqli_set_charset($mysqli, 'utf8');

		$result = $mysqli->query($query);
		return $result;
		$mysqli->close();
	}
