<?php
	session_start();
	header("Content-Type: text/html; charset=utf-8");
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/model/dbaccess.php');
	require_once($path.'/controller/main.php');

	$VERSION = '0.2.1.3';