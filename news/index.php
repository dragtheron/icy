<?php
	session_start();
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/model/dbaccess.php');
	require_once('controller/view.php');

	if (isset($_GET['action'])) $action = $_GET['action'];
	else $action = 'default';

	view($action);

	$HEADASSETS = '<link rel="stylesheet" href="css/news.css" />';

	include_once($path.'/view/main.php');
