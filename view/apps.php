<?php
	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/model/dbaccess.php');

	$apps = array();
	setApps();

	global $APP;
	if ($APP['name'] == '/' || !isset($APP['name'])) $APP = getCurrentApp(dirname($_SERVER['PHP_SELF']));


	function setApps() {
		global $apps;
		$rawApps = databaseAccess("SELECT * FROM apps ORDER BY name");
		while ($rawApp = $rawApps->fetch_assoc())
			array_push($apps, $rawApp);
	}

	function getApps() {
		global $apps;
		return $apps;
	}

	function getCurrentApp($uri) {
		global $APP;
		$app = array();
		$rawApp = databaseAccess("SELECT * FROM apps WHERE uri = '".$uri."'");
		return $rawApp->fetch_assoc();
	}

	function getHTMLNavigation() {
		global $APP;
		if (isset($_SESSION['user']))
			echo "<a class='error' href='/login/index.php?action=logout'>Logout</a>";
		$apps = getApps();
		foreach ($apps as $app) {
			echo "<a class='";
			if($app['uri'] == $APP['uri'])
				echo "active ";
			if($app['public'])
				echo "public' href='".$app["uri"]."'>";
			else
				echo "no-public' href='#'>";
			echo $app['name'];
			echo "</a>";
		}

	}
