 <?php
	/* application definition variables */
	global $APP;
	global $MAINCONTENT;
	global $ERRORCONTENT;
	global $HEADASSETS;
	global $VERSION;
	
	include 'apps.php';

  $images = array();
  $rawImages = databaseAccess("SELECT * FROM images");
  while ($rawImage = $rawImages->fetch_assoc())
    array_push($images, $rawImage);
?>

<!DOCTYPE html>
<html lang="de">
	<head>
		<!-- <base href="http://www.tstettner.de" target="_self"> -->
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>[Project Icy] <?=$APP['name']?></title>
		<meta name="description" content="Eine Sammlung von nutzerbasierten Web-Apps zum täglichen Gebrauch." />
		<meta name="keywords" content="html5, apps, webapps, daily, täglich" />
		<meta name="language" content="de" />
		<script type="text/javascript" src="/js/jquery.min.js"></script>
		<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="/js/autosize.js"></script>
		<link rel="stylesheet" href="/css/normalize.css" />
		<link rel="stylesheet" href="/css/main.css" />
		<link rel="shortcut icon" href="/favicon.ico" />
		<?=$HEADASSETS?>
	</head>
	<body style="background-image: url(/img/_blur/<?=$images[rand(0,count($images)-1)]['name']?>)">
		<noscript>
			Javascript required. Please activate Javascript in your browser settings!
		</noscript>
		<aside class='nav-closed'>
			<p id="project-title">Project Icy <span class="version"><?=isset($VERSION)?$VERSION:''?></span></p>
			<nav><?=getHTMLNavigation()?></nav>
		</aside>
		<header>
			<div id="nav-opener"></div>
			<p id="app-name">Icy <?=$APP['name']?$APP['name']:""?></p>
		</header>
    <?=isset($MENU)?$MENU:''?>
		<main class='<?=isset($MENU)?'display-menu':''?>'>
			<?=isset($ERRORCONTENT)?"<article class='error'>".$ERRORCONTENT."</article>":""?>
			<article><?=$MAINCONTENT?></article>
			<footer>
        <section>
          <a href="mailto:admin@tstettner.de"><div class='logo-behind'></div>Copyright &copy; <?=date("Y")?> <br>
  						Designed by Tobias Stettner</a>
            </section>
			</footer>
		</main>
		<script type="text/javascript" src="/js/main.js"></script>
	</body>
</html>
