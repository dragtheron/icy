<?php

	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/requirements.php');
	require_once('controller/view.php');
	include $path.'/markdown.php';

	if (isset($_GET['action'])) $action = $_GET['action'];
	else $action = 'default';

	view($action);
	$HEADASSETS = '<link rel="stylesheet" href="css/blog.css" />';

	include_once($path.'/view/main.php');
