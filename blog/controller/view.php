<?php
	global $MAINCONTENT;

	function deletePost($id) {
		databaseAccess("DELETE FROM blog WHERE id = ".$id);
		header('Location: index.php');
	}

	function getBlogPosts() {
		$posts = array();
		$rawPosts = databaseAccess("SELECT b.id as blog_id, b.title, b.content, b.changed, b.public, b.user, u.id, u.name as name FROM blog b LEFT JOIN users u ON (b.user = u.id) ORDER BY b.changed DESC");
		while ($rawPost = $rawPosts->fetch_assoc())
			array_push($posts, $rawPost);
		return $posts;
	}

	function getBlogPost($id) {
		$rawPost = databaseAccess("SELECT * FROM blog WHERE id = ".$id);
		return $rawPost->fetch_assoc();
	}

	function pushBlogPost($id, $title, $content, $public, $user) {
		if ($id != 'new')
			databaseAccess("UPDATE blog SET title = '".$title."', content = '".$content."', public = ".$public." WHERE id = ".$id);
		else
			databaseAccess("INSERT INTO blog (title, content, public, user) VALUES ('".$title."', '".$content."', ".$public.", ".$user.")");
		header('Location: index.php');
	}

	function view($action) {
		switch($action) {
			case 'compose':
				viewComposePost();
				break;
			case 'create':
				if (isset($_SESSION['user']) && adminCheck($_SESSION['user'])) {
					if (!isset($_POST['public'])) $public = 0;
					else $public = 1;
					pushBlogPost('new', $_POST['title'], $_POST['content'], $public, $_SESSION['user']);
				}
				else {
					viewError('BLOG001');
					viewMain();
				}
				break;
			case 'delete':
				if (isset($_SESSION['user']) && adminCheck($_SESSION['user']))
					deletePost($_GET['post']);
				else {
					viewError('BLOG001');
					viewMain();
				}
				break;
			case 'edit':
				if (isset($_SESSION['user']) && adminCheck($_SESSION['user']))
					viewEditPost($_GET['post']);
				else {
					viewError('BLOG001');
					viewMain();
				}
				break;
			case 'save':
				if (isset($_SESSION['user']) && adminCheck($_SESSION['user'])) {
					if (!isset($_POST['public'])) $public = 0;
					else $public = 1;
					pushBlogPost($_GET['post'], $_POST['title'], $_POST['content'], $public, $_SESSION['user']);
				}
				else {
					viewError('BLOG001');
					viewMain();
				}
				break;
			default:
				viewMain();
				break;
		}
	}

	function viewComposePost() {
		pushContent("<section class='composed'>");
			pushContent("<form method='post' action='index.php?action=create'>");
				pushContent("<input class='blog-title composer' type='text' name='title' class='blog-title ");
				pushContent("' value='' placeholder='Enter blog post title'>");
		
				pushContent("<p class='blog-subtitle'>");
				pushContent("Composed ");
				pushContent(date("F j, Y, G:i"));
				pushContent(" by <span class='link-color'>");
				pushContent($_SESSION['user_name']);
				pushContent("</span>");
				pushContent("</p>");
				
				pushContent("<textarea id='blog-content' class='blog-content composer autoresize' name='content' placeholder='Enter content'></textarea>");
				pushContent("<label for='public'>Make public </label><input name='public' type='checkbox'><br>");
				
				pushContent("<input type='submit' class='link' value='Save'>");
				pushContent("<a href='/'>Cancel</a>");
			pushContent("</form>");
		pushContent("</section>");
	}

	function viewEditPost($id) {
		$post = getBlogPost($id);
		$content = str_replace("\r<br>", "<br>\r", $post['content']);
		if (!$post['public'])
			pushContent("<section class='composed'>");
		else
			pushContent("<section>");
			pushContent("<form method='post' action='index.php?action=save&post=".$id."'>");
				pushContent("<input class='blog-title composer' type='text' name='title' class='blog-title ");
				pushContent("' value='".$post['title']."'>");
	
				pushContent("<p class='blog-subtitle'>");
				pushContent("Latest change ");
				pushContent(date("F j, Y, G:i", strtotime($post['changed'])));
				pushContent(" by <span class='link-color'>");
				pushContent($_SESSION['user_name']);
				pushContent("</span>");
				pushContent("</p>");
				pushContent("<textarea id='blog-content' class='blog-content composer autoresize' name='content'>".$content."</textarea>");
				pushContent("<label for='public'>Make public </label><input name='public' type='checkbox' value='1'");
				if ($post['public'])
					pushContent("checked");
				pushContent("><br>");
				
				pushContent("<input type='submit' class='link' value='Save'>");
				pushContent("<a href='/'>Cancel</a>");
			pushContent("</form>");
		pushContent("</section>");
	}

	function viewError($error) {
		switch($error) {
			case 'BLOG001': pushError($error, 'You do not have any privileges to edit this content. Please login first.');
		}
	}

	function viewMain() {

		$posts = getBlogPosts();
		if (isset($_SESSION['admin']) && $_SESSION['admin'])
			pushMenu("<section><p><b>Admin Panel</b>&nbsp;&nbsp;|&nbsp;&nbsp;<a href='index.php?action=compose'>Create a new post</a></p></section>");
		foreach ($posts as $post) {
			if ($post['public'] || (isset($_SESSION['admin']) && $_SESSION['admin'])) {

				pushContent("<section class='");
				if (isset($_SESSION['admin']) && $_SESSION['admin'])
					pushContent("admin ");
				if (!$post['public'])
					pushContent("composed ");
				pushContent("'>");
				pushContent("<p class='blog-title ");
				pushContent("'>");
				pushContent($post['title']);
				pushContent("</p>");

				pushContent("<p class='blog-subtitle'>");
				if (!$post['public'])
					pushContent("Composed ");
				else
					pushContent("Posted ");
				pushContent(date("F j, Y, G:i", strtotime($post['changed'])));
				pushContent(" by ");
				pushContent($post['name']);
				if (isset($_SESSION['admin']) && $_SESSION['admin']) {
					pushContent(" &mdash; <a href='index.php?action=edit&post=".$post['blog_id']."'>Edit post</a>");
					pushContent(" &mdash; <a href='index.php?action=delete&post=".$post['blog_id']."' onclick='return confirmSubmit(\"Delete post [".$post['title']."]\");'>Delete post</a>");
				}
				pushContent("</p>");

				pushContent("<p class='blog-content'>");
				$content = new Parsedown();
				pushContent($content->text($post['content']));
				pushContent("</p>");
				pushContent("</section>");
			}
		}
	}
