<?php

	$path = $_SERVER['DOCUMENT_ROOT'];
	require_once($path.'/requirements.php');

	# Init apps
	databaseAccess("CREATE TABLE `apps` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL DEFAULT '',
		  `uri` varchar(255) NOT NULL DEFAULT '',
		  `public` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	# Init blog
	databaseAccess("CREATE TABLE `blog` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `title` varchar(255) NOT NULL DEFAULT '',
		  `content` text NOT NULL,
		  `changed` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
		  `public` tinyint(1) NOT NULL DEFAULT '0',
		  `user` int(11) NOT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	# Init images
	databaseAccess("CREATE TABLE `images` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL DEFAULT '',
		  `origin` varchar(255) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	# Init news articles
	databaseAccess("CREATE TABLE `news_articles` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `title` varchar(255) NOT NULL DEFAULT '',
		  `content` text,
		  `image` varchar(255) DEFAULT '',
		  `published` timestamp NOT NULL,
		  `feed` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	# Init news feeds
	databaseAccess("CREATE TABLE `news_feeds` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL DEFAULT '',
		  `url` varchar(255) NOT NULL DEFAULT '',
		  `added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
		  `updated` timestamp DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
		  `user` int(11) DEFAULT NULL,
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");

	# Init users
	databaseAccess("CREATE TABLE `users` (
		  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
		  `name` varchar(255) NOT NULL DEFAULT '',
		  `user` varchar(255) NOT NULL,
		  `pass` varchar(512) NOT NULL DEFAULT '',
		  `admin` tinyint(1) NOT NULL DEFAULT '0',
		  PRIMARY KEY (`id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8;");



	# Create default admin (admin, icy)
	databaseAccess("INSERT INTO users (name, user, pass, admin)
		VALUES ('Admin', 'admin', 'icy', 1)");

	# Create default apps
	databaseAccess("INSERT INTO apps (name, uri, public)
		VALUES
			('Blog','/blog',1),
			('Countdown','/countdown',1),
			('News','/news',1),
			('Done','/done',0),
			('Warcraft','/warcraft',0),
			('Finance','/finance',0),
			('Calendar','/calendar',0),
			('Contact','/contacts',0),
			('Mail','/mail',0),
			('Drive','/drive',0);");



	header("Location: index.php");
