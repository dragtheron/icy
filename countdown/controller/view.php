<?php
	global $MAINCONTENT;
	global $ERRORCONTENT;

	function pushContent($content) {
		global $MAINCONTENT;
		$MAINCONTENT .= $content;
	}

	function pushError($error, $content) {
		global $ERRORCONTENT;
		$ERRORCONTENT .= "<section>";
		$ERRORCONTENT .= $content."<br><span class='identifier'>[".$error."]</span><br>";
		$ERRORCONTENT .= "</section>";
	}

	function view($action) {
		switch($action) {
			default:
				viewMain();
				break;
		}
	}

	function viewError($error) {
		switch($error) {
			default :
				pushError('ERROR', 'An unknown error occurred.');
				break;
		}
	}

	function viewMain() {
	}
